#!/usr/bin/env bash
# set -euo pipefail
set -eo pipefail
IFS=$'\n\t'
#........................................................................
#    Executable script to set up DIRAC server and client instances with
#        ElasticSearch and MySQL services, all in docker containers.
#
#    The following software is required on top of Cern Centos 7 (CC7):
#      * Docker v18+
#      * Docker-Compose v2.4+
#
#    For the script to run, the shell must be logged into the CERN
#    container registry at gitlab-registry.cern.ch using
#    `docker login gitlab-registry.cern.ch` and following the prompts
#
#    Edit environment variables (settings) in the CONFIG file
#........................................................................

BUILD_DIR=$PWD
SCRIPT_DIR="$( cd "$( dirname "$0" )" >/dev/null 2>&1 && pwd )"

if [ -z "$TMP" ]; then
    TMP=/tmp/DIRAC_CI_$(date +"%Y%m%d%I%M%p")
    mkdir -p "$TMP"
fi
if [ -z "$CLIENTCONFIG" ]; then
    CLIENTCONFIG=$TMP/CLIENTCONFIG
fi
if [ -z "$SERVERCONFIG" ]; then
    SERVERCONFIG=$TMP/SERVERCONFIG
fi

# shellcheck source=tests/CI/CONFIG
source "$SCRIPT_DIR/CONFIG"
# shellcheck source=tests/CI/utils.sh
source "$SCRIPT_DIR/utils.sh"
cd "$SCRIPT_DIR"

parseArguments

docker-compose -f ./docker-compose.yml up -d

echo -e "\n**** $(date -u) Creating user and copying scripts ****"

# DIRAC server user and scripts
docker exec server adduser -s /bin/bash -d "$USER_HOME" "$USER"
docker exec client adduser -s /bin/bash -d "$USER_HOME" "$USER"

# Create database user
docker exec mysql mysql --password=password -e "CREATE USER '${DEFAULT_DB_USER}'@'%' IDENTIFIED BY '${DEFAULT_DB_PASSWORD}';"
docker exec mysql mysql --password=password -e "CREATE USER '${DEFAULT_DB_USER}'@'localhost' IDENTIFIED BY '${DEFAULT_DB_PASSWORD}';"
docker exec mysql mysql --password=password -e "CREATE USER '${DEFAULT_DB_USER}'@'mysql' IDENTIFIED BY '${DEFAULT_DB_PASSWORD}';"

docker cp ./install_server.sh server:"$WORKSPACE"
docker cp ./install_client.sh client:"$WORKSPACE"

copyLocalSource server "${SERVERCONFIG}"
copyLocalSource client "${CLIENTCONFIG}"

docker cp "$SERVERCONFIG" server:"$WORKSPACE/CONFIG"
docker cp "$CLIENTCONFIG" client:"$WORKSPACE/CONFIG"

echo -e "\n**** $(date -u) Installing DIRAC server ****"
docker exec -u root server yum install -y freetype
docker exec -u "$USER" -w "$WORKSPACE" server bash ./install_server.sh 2>&1 | tee "${BUILD_DIR}/log_server_install.txt"

echo -e "\n**** $(date -u) Copying credentials and certificates ****"
docker exec client bash -c "mkdir -p $WORKSPACE/ServerInstallDIR/user $WORKSPACE/ClientInstallDIR/etc /home/dirac/.globus"
docker cp server:"$WORKSPACE/ServerInstallDIR/etc/grid-security" - | docker cp - client:"$WORKSPACE/ClientInstallDIR/etc/"
docker cp server:"$WORKSPACE/ServerInstallDIR/user/client.pem" - | tee  >/dev/null \
  >(docker cp - client:"$USER_HOME/.globus/") \
  >(docker cp - client:"$WORKSPACE/ServerInstallDIR/user/client.pem")
docker cp server:"$WORKSPACE/ServerInstallDIR/user/client.key" - | tee  >/dev/null \
  >(docker cp - client:"$USER_HOME/.globus/") \
  >(docker cp - client:"$WORKSPACE/ServerInstallDIR/user/client.key")
docker cp server:/tmp/x509up_u1000 - | docker cp - client:/tmp/x509up_u1000
docker exec client bash -c "chown -R dirac:dirac /home/dirac /tmp/x509up_u1000"

echo -e "\n**** $(date -u) Installing client ****"
docker exec -u "$USER" -w "$WORKSPACE" client bash ./install_client.sh 2>&1 | tee "${BUILD_DIR}/log_client_install.txt"

echo -e "\n**** $(date -u) Starting server tests ****"
docker exec -u "$USER" -w "$WORKSPACE" -e INSTALLROOT="$WORKSPACE" -e INSTALLTYPE=server server \
    bash TestCode/DIRAC/tests/CI/run_tests.sh || SERVER_ERR=$?

echo -e "\n**** $(date -u) Starting client tests ****"
docker exec -u "$USER" -w "$WORKSPACE" -e INSTALLROOT="$WORKSPACE" -e INSTALLTYPE=client client \
    bash TestCode/DIRAC/tests/CI/run_tests.sh || CLIENT_ERR=$?

echo -e "\n**** $(date -u) ALL DONE ****"
if [ ${SERVER_ERR:-0} -eq 0 ] && [ ${CLIENT_ERR:-0} -eq 0 ]; then
    echo "SUCCESS: All tests succeeded"
    exit 0
else
    echo "At least one unit test failed. Check the logs for more info. "
    exit 1
fi
